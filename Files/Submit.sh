#!/bin/bash
#This script will take users arguments and run spark-submit
#Version 1.0
#July 14, 2017

function SearchMain {
	unzip -d /jars/Temp $Location
	ClassName=$(grep Main-Class /jars/Temp/META-INF/MANIFEST.MF | awk {'print $2'})
	echo "Found Main class to be: $ClassName"
} 


if [ -z "$ClassName" ]
	then
	echo "No Main Class name entered"
	echo "Next time set $ClassName with Main class name"
# Check if location of jar is set
	if [ -z "$Location" ]
		then
		echo "No location of jar entered"
		echo "Next time set $Location with location of jar file"
		exit 1
	else
	#go to function to get main class
		SearchMain		
	fi
fi

Date=$(date +%F-%T).txt
echo $ClassName $Location $Date> /output/$Date
spark-submit --class="$ClassName" $Location | tee -a /output/$Date

#docker run -itP -e ClassName="SparkPi" -e Location="/jars/spark-sample_2.11-1.0.jar" -v /home/dshan/Work/sparkstand/jars/:/jars -v /home/dshan/Work/sparkstand/output/:/output  sparkstand 
