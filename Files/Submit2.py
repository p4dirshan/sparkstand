#!/usr/bin/python
#This script will take users arguments and run spark-submit
#Version 1.1 Reordered code and save spark-submit to output file and display on terminal
#Version 1.0
#July 27, 2017
import os, zipfile, datetime, subprocess, shlex, shutil

# Check if Location of jar is set
try:
	print os.environ['Location']
	Location=os.environ['Location']
except KeyError:
	print "No location of jar entered"
	print "Next time set $Location with location of jar file"
	exit()

# Check if ClassName is set
try:
	print os.environ['ClassName']
	ClassName=os.environ['ClassName']
except KeyError:
	print "No Main Class name entered"
	print "Next time set $ClassName with Main class name"
	# find the main class from location of jar file
	jarfile = zipfile.ZipFile(os.environ['Location'], 'r')
	jarfile.extractall("/jars/Temp")           
	jarfile.close()
	dataFile = file('/jars/Temp/META-INF/MANIFEST.MF')
	for line in dataFile:
		if "Main-Class" in line:
			ClassName=line.split()[-1]
	print "Main Class name is found to be: " + ClassName
	#Cleanup Temp Folders
	shutil.rmtree('/jars/Temp/')
Date=datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".txt"
teeCommand="tee /output/"+Date
#subprocess.call(["spark-submit", "--class", ClassName, os.environ['Location'], "2>&1", "|", "tee /output/$(date +%F-%T).txt"])
#ClassName=os.environ['ClassName']
#Location=os.environ['Location']

Run="spark-submit --class"+" "+ClassName+" "+Location
SparkProcess=subprocess.Popen(shlex.split(Run),stdout=subprocess.PIPE)
OutPutProcess=subprocess.Popen(shlex.split(teeCommand), stdin=SparkProcess.stdout,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
SparkProcess.stdout.close()
out,err=OutPutProcess.communicate()
print('output: {0}'.format(out))
print('errors: {0}'.format(err))
