#!/bin/bash
#This script will setup master and worker nodes for Apache Spark Docker Image
#Version 1.1 Added argument to script for MasterIP
#Version 1.0
#August 3, 2017

function slaveSetup(){
#check if first argument is present, first argument is used for MasterIP
if [[ -z $MasterIP ]]; then
	#ask for master IP address
	while true; do
		read -p "What is master IP address?" MasterIP
		if [ -z $MasterIP ]; then
			echo "Please enter in the IP address of Master Spark node"
		else
			break
		fi
	done
fi

#test connection to Master node
if telnet $MasterIP 7077 </dev/null 2>&1 | grep -q Escape; then
echo "Can Connect to Master";
else echo "Can not connect to Master"; exit;
fi
}

#Beginning of script
#Setup for master or worker
#if first argument is set, the set MasterIP to IP address of master node
MasterIP=$1
while true; do
    read -p "Setup for master[Y/n]?" yn
    case $yn in
        [Yy]* ) echo "Setting up Master Node!!"; Choice="Master";break;;
        [Nn]* ) Choice="Slave";slaveSetup;break;;
        * ) echo "Please answer yes or no.";;
    esac
done

#check to see if ports are free

if [ "$(sudo netstat -tupln | grep -Ew '8080|7077' | awk {'print $7'} | wc -l)" == "0" ]; then
echo "No Application is using the required ports"
else
sudo netstat -tupln | grep -Ew '8080|7077' | awk {'print $7'}
echo "Please stop the running applications PIDs as these ports are required"
exit
fi

#check if spark-env.sh exists
if [ -z "/usr/local/spark*/conf/spark-env.sh" ]; then
echo " spark-env.sh does not exists, creating file"
cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
fi

#get ip address
IP=$(ifconfig eth0 | grep -w inet | awk {'print $2'} | tr -d 'addr:')
echo "Your IP address is: " $IP

if [ "$Choice" == "Master" ]; then
	#append data to spark-env.sh file
	echo "export SPARK_MASTER_HOST='$IP'" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
	echo "export SPARK_LOCAL_IP='$IP'" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh

	#start master
	echo "starting Spark-Master"
	/usr/local/spark-2.1.1-bin-hadoop2.7/sbin/start-master.sh

	#echo out run command for worker node
	echo "On worker nodes enter: /usr/local/spark-2.1.1-bin-hadoop2.7/sbin/start-slave.sh spark://$IP:7077"
	echo "Or run DistributeSetup.sh $IP"
	echo "Open your broswer to $IP:8080"
elif [ "$Choice" == "Slave" ]; then
	#append data to spark-env.sh file
	echo "export SPARK_MASTER_HOST='$MasterIP'" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
	echo "export SPARK_LOCAL_IP='$IP'" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh

	#start slave
	echo "starting worker node"
	/usr/local/spark-2.1.1-bin-hadoop2.7/sbin/start-slave.sh spark://$MasterIP:7077
	echo "Open your broswer to $MasterIP:8080"
fi
